const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();
const mysql = require('mysql');

var connection =mysql.createConnection({
    host : "localhost",
    user : "root",
    password : "root",
    database : "Kapo"
});
app.set("view engine", "pug");
app.set("views", __dirname+ "/views");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

app.get('/imenik', function(req, res) {
    connection.query('SELECT * FROM imenik', function (err, data, kom) {
        if(!err) {
            console.log(data);
            res.render("tabela", {redovi : data});
            
        }
    });
});

app.post('/imenik', function(req,res) {
    let ime = req.body["ime"];
    let adresa = req.body["adresa"];
    let telefon = req.body["telefon"];
    connection.query('INSERT INTO imenik (id, ime, adresa, telefon) VALUES (NULL,"'+ ime +'","'+adresa+'","'+telefon+'")', function (err, data, kom) {
        if(!err) {
            res.json({message:"radi"});
        }
        else
        res.json({message:err.message});
    });
});

app.get('/poznanik/:kontakt', function(req, res) {
    let kontakt = req.params.kontakt;
    connection.query('SELECT i.ime,i.adresa,i.telefon FROM imenik i, adresar a WHERE i.id=a.idPoznanik and a.idKontakta="' + kontakt+'"', function (err, data, kom) {
        if(!err) {
            res.render("tabela", {redovi : data});
            
        }
    });

});


app.listen(3000);